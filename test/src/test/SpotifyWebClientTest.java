package test;

import java.io.File;
import java.io.IOException;
import static junit.framework.Assert.assertNotNull;
import junit.framework.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.sikuli.api.DesktopScreenRegion;
import org.sikuli.api.ImageTarget;
import org.sikuli.api.ScreenRegion;
import org.sikuli.api.Target;
import org.sikuli.api.robot.Key;
import org.sikuli.api.robot.Keyboard;
import org.sikuli.api.robot.Mouse;
import org.sikuli.api.robot.desktop.DesktopKeyboard;
import org.sikuli.api.robot.desktop.DesktopMouse;
import org.sikuli.api.robot.desktop.DesktopScreen;
import org.sikuli.api.visual.Canvas;
import org.sikuli.api.visual.DesktopCanvas;
public class SpotifyWebClientTest {

	public static void main(String[] args) throws IOException,InterruptedException {
		
		final String rightUserName ="hadi1315";
		final String rightPassword ="camcam32";
		//final String wrongUserName ="ABCD";
		//final String wrongPassword ="1234";
				
		
		
		// Instantiate selenium webdriver
		
		WebDriver driver = new FirefoxDriver();
		//driver.manage().window().maximize();
		driver.get("https://play.spotify.com/");
		
	
		
		//Instantiate the sikuli infarastructure (API)
		//Find login hyperlink on Screen
		ScreenRegion s = new DesktopScreenRegion();
		Target target = new ImageTarget(new File("images/loginhere.png"));
		ScreenRegion r = s.wait(target, 5000);
		//r =s.find(target);
		
		
		//Display massages when an element is found
		Canvas canvas = new DesktopCanvas();
		//canvas.addBox(r);
		canvas.addLabel(r, "Have found login").display(1);
		//Instantiate mouse and keybord objects for using them click/writ on found element
		Mouse mouse = new DesktopMouse();
		//mouse.click(r.getCenter());
		mouse.doubleClick(r.getCenter());
		Keyboard kb = new DesktopKeyboard();
		
		
		//**********Enter User name
		//target = new ImageTarget(new File("/Users/home/Documents/workspace/test/images/username.png"));
		target = new ImageTarget(new File("images/username.png"));
		//r =s.find(target);
		//mouse.click(r.getCenter());
		
		/*canvas.addLabel(r, "Have found user name");
		canvas.display(1);*/
		//kb.type("hadi1315");
		kb.type(rightUserName);
		
		//***********Enter Pw
		target = new ImageTarget(new File("images/password.png"));
		r =s.find(target);
		//canvas.addLabel(r, "Have found pw field");
		//canvas.display(1);
		//r = s.wait(target, 2000);
		mouse.click(r.getCenter());
		//kb.type("camcam32");
		kb.type(rightPassword);
		//**************Login 
		target = new ImageTarget(new File("images/login.png"));
		r =s.find(target);
		//canvas.addLabel(r,"Have found login-button");
		//canvas.display(1);
		mouse.doubleClick(r.getCenter());
		
		//***************Search
		r = s.wait(target, 5000);
		target = new ImageTarget(new File("images/search.png"));
		r =s.find(target);
		//canvas.addLabel(r, "Have found search").display(1);
		mouse.doubleClick(r.getCenter());
		
		
		//****************Tnsert text into Search Box
		//r = s.wait(target, 5000);
		target = new ImageTarget(new File("images/searchbox.png"));
		r =s.find(target);
		r = s.wait(target, 5000);
		//canvas.addLabel(r, "Have found search-box").display(1);
		//mouse.doubleClick(r.getCenter());
		kb.type("Bob Dylan\n");
		//kb.paste("Bob Dylan");
		//kb.type(Key.ENTER);
		
		// *****************Select a Album called Top tracklist
		//r = s.wait(target, 5000);
		target = new ImageTarget(new File("images/toptracklist.png"));
		r =s.find(target);
		//canvas.addLabel(r, "Have found tracklist").display(1);
		//mouse.doubleClick(r.getCenter());
		r = s.wait(target, 5000);
		mouse.doubleClick(r.getCenter());
		
		
		//********************Play the tracklist
		//r = s.wait(target, 5000);
		target = new ImageTarget(new File("images/playtoptracklist.png"));
		r =s.find(target);
		r = s.wait(target, 3000);
		mouse.doubleClick(r.getCenter());

		
		//********************Pause/stop the tracklist:(
				target = new ImageTarget(new File("images/pause.png"));
				r =s.find(target);
				r = s.wait(target, 5000);
				mouse.click(r.getCenter());
		
		//******************Go to setting (for logout)	
		
		r = s.wait(target, 3000);
		target = new ImageTarget(new File("images/settings.png"));
		r =s.find(target);
		//canvas.addLabel(r, "Have found settings").display(3);
		//mouse.doubleClick(r.getCenter());
		mouse.doubleClick(r.getCenter());
		
		
		//**********************Logout
		
		
		
		/*target = new ImageTarget(new File("images/logout.png"));
		
		r =s.find(target);
		//r = s.wait(target, 5000);
		//canvas.addLabel(r, "Have found logout").display(1);
		mouse.doubleClick(r.getCenter());*/
		
		
	}

}

