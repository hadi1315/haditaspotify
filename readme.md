Subject: Evaluation of test task

Reference: Konstantin Bitsakis



Hello Spotifire :)

I am a tester and have applied for QA- role in Spotify. This repo is an attempt to automate the spotify’s web client. The task was to write auto-test but i have not been successful with that:(

I have not been successful with maven integration regarding the images (*.png). I received classloader exceptions when loading the png files and was not able to resolve it:(

Selenium ,sikuli api library and other dependencies waere managed manually in eclipse project.There is neither assertion for testing nor refactoring of the source code.

The supported browser is firefox. It might behaves differently depending on the initial page size of browser.
Unfortunately, there is no test code, it should have fallowed AAA with setup/teardown of initialization or global resources using junit.

To run this simple gui automation, run the generated spotify-client.jar in the same folder (test folder). Sometimes it halts due to the ads updating the browser during running the test. Please re-run the test to give it a chance to succeed

I am no satisfied with my result and loading images with maven resources (I include my pom.xml file). Most of my time was spending on environment.
I was not able to get the “logout” button to work, no matter how many times I recaptured the image.

It was a very well structured and good assignment.


Thanks in advance

Hadi Hadidi

Hadi.hadidi@gmail.com
